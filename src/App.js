import React, { Component } from 'react';
import { ApolloClient, gql, graphql, ApolloProvider, createNetworkInterface } from 'react-apollo';
import './App.css';
import RootComponent from './components/RootComponent'


const client = new ApolloClient({
  networkInterface: createNetworkInterface({
    uri: 'https://api.graph.cool/simple/v1/cj3aab8m2f6qz0182y9lliztm'
  })
});

const query = gql`
   query AppQuery($offerSortBy: OfferOrderBy!, $priceFilter: Float!, $deductibleFilter: [Int!], $companyFilter: [ID!]){
    allCompanies(orderBy: name_ASC) {
      id,
      name
    },
    allOffers(
      orderBy: $offerSortBy,
      filter: {
        AND: [{
          price_lte: $priceFilter
        },{
          deductible_in: $deductibleFilter
        },
        {
          company: {
            id_in: $companyFilter
          }
        }] 
      }) {
      id,
      price,
      deductible,
      company {
        name,
        imageUrl
      }
    }
  }`;
const MyApp = graphql(query, {
  options : {
    variables : {
      offerSortBy: 'createdAt_DESC',
      priceFilter: 50,
      deductibleFilter: [0, 3, 5, 7],
      companyFilter: ['cj3aaih45f1ee0157fq8acf7b', 
                        'cj3aaitarfuls01767wrxmykf', 
                        'cj3aaj8wafuon0176a9innrjn', 
                        'cj3aakw7zfgyx0165stccde70']
    }
  }
})(RootComponent);

class App extends Component {
  render() {
    return (
      <ApolloProvider client={client}>
        <div className="App">
          <MyApp />
        </div>
      </ApolloProvider>
    );
  }
}

export default App;
