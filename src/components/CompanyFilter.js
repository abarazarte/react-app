import React, { Component } from 'react'
import { Row, Col } from 'react-bootstrap'

class CompanyFilter extends Component {
    constructor(props) {
        super(props);
        this.handleCompanyFilterInputChange = this.handleCompanyFilterInputChange.bind(this);
        this.companiesId =  this.props.companies.map(company => {
            return company.id;
        })
    }

    handleCompanyFilterInputChange(e) {
      this.props.onCompanyFilterInputChange(e.target.value);
    }
    render() {
        const ulStyle = {
            listStyle : "none",
            paddingLeft: 0
        }
        const liStyle = {
            textAlign : "left"
        }

        return <Row>
            <Col sm={12} style={liStyle}>
                <h5><strong>Companies</strong></h5>
                <ul style={ulStyle}>
                { this.props.alLCompanies.map( company =>
                    <li key={company.id} style={liStyle}>
                    <label>
                    <input key={company.id}
                        name="{company.name}"
                        type="checkbox" 
                        value={company.id}
                        onChange={this.handleCompanyFilterInputChange}
                        checked={this.props.companies.indexOf(company.id) > -1}/>
                        {company.name}
                    </label>
                    </li>
                )}
                </ul>
            </Col>
        </Row>;
    }
}

export default CompanyFilter;