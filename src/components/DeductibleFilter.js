import React, { Component } from 'react'
import { Row, Col } from 'react-bootstrap'

class DeductibleFilter extends Component {
    constructor(props){
        super(props);
        this.handleDeductiblesFilterInputChange = this.handleDeductiblesFilterInputChange.bind(this);
    }

    handleDeductiblesFilterInputChange(e) {
      this.props.onDeductibleFilterInputChange(e.target.value);
    }

    render(){
        const alingLeft = {
            textAlign : "left"
        }
        return <Row>
            <Col sm={12}>
                <Row>
                    <Col sm={12} style={alingLeft}>
                        <h5><strong>Deductibles</strong></h5>
                    </Col>
                </Row>
                <Row>
                    <Col xs={3}>
                        <label>
                            <input type="checkbox" name="vehicle" value="0"
                            key="day_0"
                            onChange={this.handleDeductiblesFilterInputChange}
                            checked={this.props.deductiblesFilter.indexOf(0) > -1}/>    0
                        </label>
                    </Col>
                    <Col xs={3}>
                        <label>
                            <input type="checkbox" name="vehicle" value="3"
                            id="day_3"
                            onChange={this.handleDeductiblesFilterInputChange}
                            checked={this.props.deductiblesFilter.indexOf(3) > -1}/>    3
                        </label>
                    </Col>
                    <Col xs={3}>
                        <label>
                            <input type="checkbox" name="vehicle" value="5"
                            id="day_5"
                            onChange={this.handleDeductiblesFilterInputChange}
                            checked={this.props.deductiblesFilter.indexOf(5) > -1}/>    5
                        </label>
                    </Col>
                    <Col xs={3}>
                        <label>
                            <input type="checkbox" name="vehicle" value="7"
                            id="day_7"
                            onChange={this.handleDeductiblesFilterInputChange}
                            checked={this.props.deductiblesFilter.indexOf(7) > -1}/>    7
                        </label>
                    </Col>
                </Row>
            </Col>
        </Row>;
    }
}

export default DeductibleFilter;