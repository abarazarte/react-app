import React, { Component } from 'react'
import { Row, Col } from 'react-bootstrap'

class Offer extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const boderStyle = {
            border: "1px solid black"
        };
        const marginBottom = {
            marginBottom: "10px"
        }
        const imgStyle = {
            paddingTop: "15px",
            paddingLeft: "15px",
            margin: "0 auto",
            maxHeight: "100px"
        }
        const borderRight = {
            borderRight: "1px solid black"
        }
        const rowStyle = {
            display: 'flex'
        }
        const contentStyle= {
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'center'
        }

        return <Row style={marginBottom}>
            <Col sm={12}>
                <div style={boderStyle}>
                    <Row style={rowStyle}>
                        <Col xs={5} style={borderRight}>
                            <img src={this.props.company.imageUrl} alt={this.props.company.name} className="img-responsive" style={imgStyle}/>
                            <p>{this.props.company.name}</p>
                        </Col>
                        <Col xs={7} className="text-left" style={contentStyle}>
                            <p><strong>Price:</strong> {this.props.price}</p>
                            <p><strong>Deductible:</strong> {this.props.deductible}</p>
                        </Col>
                    </Row>
                </div>
            </Col>
        </Row>;
    }
}

export default Offer;