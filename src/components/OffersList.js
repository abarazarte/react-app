import React, { Component } from 'react';
import { Row, Col } from 'react-bootstrap'
import Offer from './Offer'

class OffersList extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return <Row>
            <Col sm={12}>
                { this.props.offers.map( offer =>
                    <Offer key={offer.id} {...offer}/>
                )}
            </Col>
        </Row>;
    }
}

export default OffersList;