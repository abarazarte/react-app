import React, { Component } from 'react'
import { Row, Col } from 'react-bootstrap'

class OffersListHeader extends Component {
    constructor(props) {
        super(props);
        this.handleSortByInputChange = this.handleSortByInputChange.bind(this);
    }

    handleSortByInputChange(e) {
      this.props.onSortByInput(e.target.value);
    }

    render() {
        return <Row>
            <Col xs={4} className="text-left">
                <p>{this.props.count} results</p>
            </Col>
            <Col xs={8} className="text-right">
                <select value={this.props.sortBy}
                    onChange={this.handleSortByInputChange}>
                    <option defaultValue value="createdAt_DESC">Sort by...</option>
                    <option value="deductible_ASC">Deductible: ASC</option>
                    <option value="deductible_DESC">Deductible: DESC</option>
                    <option value="price_ASC">Price: ASC</option>
                    <option value="price_DESC">Price: DESC</option>
                </select>
            </Col>
        </Row>;
    }
}

export default OffersListHeader;