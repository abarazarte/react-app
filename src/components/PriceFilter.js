import React, { Component } from 'react'
import { Row, Col } from 'react-bootstrap'

class PriceFilter extends Component {
    constructor(props) {
        super(props);
        this.handlePriceFilterInputChange = this.handlePriceFilterInputChange.bind(this);
    }

    handlePriceFilterInputChange(e) {
        this.props.onPriceFilterInputChange(e.target.value);
    }

    render() {
        const alingLeft = {
            textAlign : "left"
        }

        return <Row>
            <Col sm={12} style={alingLeft}>
                <Row>
                    <Col sm={12}>
                        <h5><strong>Price ranges</strong></h5>
                        <input type="range" min="0" max="100" step="50"
                            value={this.props.priceFilter}
                            onChange={this.handlePriceFilterInputChange}/>
                    </Col>
                </Row>
                <Row>
                    <Col xs={4}>0</Col>
                    <Col xs={4} className="text-center">50</Col>
                    <Col xs={4} className="text-right">100</Col>
                </Row>
            </Col>
        </Row>;
    }
}

export default PriceFilter;