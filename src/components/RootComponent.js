import React, { Component } from 'react'
import { Grid, Row, Col } from 'react-bootstrap';
import CompanyFilter from './CompanyFilter'
import PriceFilter from './PriceFilter'
import DeductibleFilter from './DeductibleFilter'
import OffersList from './OffersList'
import OffersListHeader from './OffersListHeader'
import logo from '../logo.svg';
import '../App.css';

class RootComponent extends Component {
    constructor(props){
        super(props);
        this.state = {
            sortBy : 'createdAt_DESC',
            priceFilter : 50,
            deductibleFilter: [0, 3, 5, 7],
            companyFilter: ['cj3aaih45f1ee0157fq8acf7b', 
                        'cj3aaitarfuls01767wrxmykf', 
                        'cj3aaj8wafuon0176a9innrjn', 
                        'cj3aakw7zfgyx0165stccde70']
        }
        this.handleSortByInput = this.handleSortByInput.bind(this);
        this.handlePriceFilterInput = this.handlePriceFilterInput.bind(this);
        this.handleDeductibleFilterInput = this.handleDeductibleFilterInput.bind(this);
        this.handleCompanyFilterInput = this.handleCompanyFilterInput.bind(this);
    }

    handleSortByInput(sortBy) {
        this.setState({
            sortBy: sortBy,
            priceFilter : this.state.priceFilter,
            deductibleFilter: this.state.deductibleFilter,
            companyFilter: this.state.companyFilter
        });
        
        this.props.data.refetch({
            offerSortBy: sortBy,
            priceFilter: parseInt(this.state.priceFilter),
            deductibleFilter: this.state.deductibleFilter,
            companyFilter: this.state.companyFilter
        });
    }

    handlePriceFilterInput(price) {
        this.setState({
            sortBy: this.state.sortBy,
            priceFilter : price,
            deductibleFilter: this.state.deductibleFilter,
            companyFilter: this.state.companyFilter
        });
        this.props.data.refetch({
            offerSortBy: this.state.sortBy,
            priceFilter: parseInt(price),
            deductibleFilter: this.state.deductibleFilter,
            companyFilter: this.state.companyFilter
        });
    }

    handleDeductibleFilterInput(deductible) {
        var arr = this.state.deductibleFilter;
        var idx = arr.indexOf(parseInt(deductible));
        if(idx > -1){
            arr.splice(idx, 1);
        }
        else {
            arr.push(parseInt(deductible))
        }
        this.setState({
            sortBy: this.state.sortBy,
            priceFilter : this.state.priceFilter,
            deductibleFilter: arr,
            companyFilter: this.state.companyFilter
        });
         this.props.data.refetch({
            offerSortBy: this.state.sortBy,
            priceFilter: parseInt(this.state.priceFilter),
            deductibleFilter: arr,
            companyFilter: this.state.companyFilter
        });
    }

    handleCompanyFilterInput(company) {
        var arr = this.state.companyFilter;
        var idx = arr.indexOf(company);
        if(idx > -1){
            arr.splice(idx, 1);
        }
        else {
            arr.push(company)
        }
        this.setState({
            sortBy: this.state.sortBy,
            priceFilter : this.state.priceFilter,
            deductibleFilter: this.state.deductibleFilter,
            companyFilter: arr
        });
         this.props.data.refetch({
            offerSortBy: this.state.sortBy,
            priceFilter: parseInt(this.state.priceFilter),
            deductibleFilter: this.state.deductibleFilter,
            companyFilter: arr
        });
    }

    render() {
        const { data: { loading, error, allCompanies, allOffers } } = this.props;

        if (loading) {
            return <img src={logo} className="App-logo" alt="logo" />;
        }
        if (error) {
            return <p>{error.message}</p>;
        }

        const filterStyle = {
            border: "1px solid black",
            padding: "15px"
        }

        const marginTop = {
            marginTop: "20px" 
        }

        return <Grid>
                    <Row>
                    <Col sm={4} style={marginTop}>
                        <div style={filterStyle}>
                        <CompanyFilter alLCompanies={allCompanies}
                            onCompanyFilterInputChange={this.handleCompanyFilterInput}
                            companies={this.state.companyFilter}/>
                        <PriceFilter onPriceFilterInputChange={this.handlePriceFilterInput}
                            priceFilter={this.state.priceFilter}/>
                        <DeductibleFilter onDeductibleFilterInputChange={this.handleDeductibleFilterInput} 
                            deductiblesFilter={this.state.deductibleFilter}/>
                        </div>
                    </Col>
                    <Col sm={8} style={marginTop}>
                        <OffersListHeader count={allOffers.length}
                            onSortByInput={this.handleSortByInput}
                            sortBy={this.state.sortBy}/>
                        <OffersList offers={allOffers}/>
                    </Col>
                    </Row>
                </Grid>;
    }
}

export default RootComponent;